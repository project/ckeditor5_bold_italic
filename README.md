# CKEditor5 Bold and Italic

This module converts <strong> and <em> tag to <b> and <i> respectively.

For a full description of the module, visit the
[project page](https://www.drupal.org/project/ckeditor5_bold_italic).

Submit bug reports and feature suggestions, or track changes in the
[issue queue](https://www.drupal.org/project/issues/ckeditor5_bold_italic).


## Table of contents

- [Requirements](#requirements)
- [Installation](#installation)
- [Configuration](#configuration)
- [Maintainers](#maintainers)

## Requirements

This module does not have any dependency on any other module.

## Installation

Install as you would normally install a contributed Drupal module. For further
information, see
[Installing Drupal Modules](https://www.drupal.org/docs/extending-drupal/installing-drupal-modules).

**Composer**
If you use Composer, you can install 'CKEditor5 Bold and Italic' module using below command:
```bash
composer require drupal/ckeditor5_bold_italic
```

## Configuration

The module has no menu or modifiable settings. There is no configuration.

## Maintainers

- Siva Karthik Reddy Papudippu - [sivakarthik229](https://www.drupal.org/u/sivakarthik229)
