// webpack.config.js

'use strict';

const path = require('path');
const { styles } = require('@ckeditor/ckeditor5-dev-utils');

module.exports = {
    entry: './src/index.js',

    output: {
        path: path.resolve(__dirname, 'dist'),
        filename: 'bundle.js'
    },

    module: {
        rules: [
            {
                test: / theme[ / \\]icons[ / \\][^ / \\] + \.svg$ / ,
                use: [ 'raw-loader' ]
            },
            {
                test: / theme[/\\].+\.css$/,
            }
        ]
    },

    devtool: 'source-map',
    performance: { hints: false }
};
