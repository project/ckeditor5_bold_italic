// cspell:ignore drupalemphasis

import DrupalBold from './drupalbold';

/**
 * @private
 */
export default {
  DrupalBold,
};
