/* eslint-disable import/no-extraneous-dependencies */
import { Plugin } from 'ckeditor5/src/core';

/**
 * Alters the italic command to output `<em>` instead of `<i>`.
 *
 * @private
 */
class DrupalBoldEditing extends Plugin {
  /**
   * @inheritdoc
   */
  static get pluginName() {
    return 'DrupalBoldEditing';
  }

  /**
   * @inheritdoc
   */
  init() {
    this.editor.conversion.for('downcast').attributeToElement({
      model: 'bold',
      view: 'b',
      converterPriority: 'high',
    });
  }
}

export default DrupalBoldEditing;
