/* eslint-disable import/no-extraneous-dependencies */
// cspell:ignore drupalemphasisediting

import { Plugin } from 'ckeditor5/src/core';
import DrupalBoldEditing from './drupalboldediting';

/**
 * Drupal-specific plugin to alter the CKEditor 5 italic command.
 *
 * @private
 */
class DrupalBold extends Plugin {
  /**
   * @inheritdoc
   */
  static get requires() {
    return [DrupalBoldEditing];
  }

  /**
   * @inheritdoc
   */
  static get pluginName() {
    return 'DrupalBold';
  }
}

export default DrupalBold;
